/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ernest.kufuor;
//import 

import ernest.kufuor.MarksView.Student;
import java.util.ArrayList;

/**
 *
 * @author HP
 */
public class Structure {

    MarksView.Student student;
    String firstName;
    String surname;
    int yearGroup;
    int iD;
    float marks;
    Major program;

    enum Major {

        BA, MIS, CS, CE, EE, ME
    };
    ArrayList<MarksView.Student> myArray = new ArrayList<MarksView.Student>();

    public void init(String name, String sName, int yGroup, int sID, float mark, String course) {
        firstName = name;
        surname = sName;
        yearGroup = yGroup;
        iD = sID;
        marks = mark;
        program = MajorConverter(course);
        student = new MarksView.Student();
        intitialize();
        myArray.add(student);
    }

    public Major MajorConverter(String course) {
        if (course.equalsIgnoreCase("BA")) {
            return program = Major.BA;
        } else if (course.equalsIgnoreCase("CS")) {
            return program = Major.CS;
        } else if (course.equalsIgnoreCase("MIS")) {
            return program = Major.MIS;
        } else if (course.equalsIgnoreCase("CE")) {
            return program = Major.CE;
        } else if (course.equalsIgnoreCase("EE")) {
            return program = Major.EE;
        } else if (course.equalsIgnoreCase("ME")) {
            return program = Major.ME;
        } else {
            return null;
        }
    }

    public void intitialize() {
        student.firstName = firstName;
        student.surname = surname;
        student.yearGroup = yearGroup;
        student.iD = iD;
        student.marks = marks;
        // student.program = program;  
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return yearGroup;
    }

    public float getMarks() {
        return marks;
    }

    public int getID() {
        return iD;
    }

    public String getCourse() {
        String prog;
        switch (program) {
            case BA:
                return prog = "BA";
            case CS:
                return prog = "CS";
            case MIS:
                return prog = "MIS";
            case CE:
                return prog = "CE";
            case ME:
                return prog = "ME";
            case EE:
                return prog = "EE";
            default:
                return prog = null;
        }

    }
}
